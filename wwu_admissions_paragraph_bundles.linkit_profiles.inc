<?php
/**
 * @file
 * wwu_admissions_paragraph_bundles.linkit_profiles.inc
 */

/**
 * Implements hook_default_linkit_profiles().
 */
function wwu_admissions_paragraph_bundles_default_linkit_profiles() {
  $export = array();

  $linkit_profile = new LinkitProfile();
  $linkit_profile->disabled = FALSE; /* Edit this to true to make a default linkit_profile disabled initially */
  $linkit_profile->api_version = 1;
  $linkit_profile->name = 'wwu_admissions_link_field';
  $linkit_profile->admin_title = 'WWU Admissions Link Field';
  $linkit_profile->admin_description = '';
  $linkit_profile->profile_type = '2';
  $linkit_profile->data = array(
    'button_text' => 'Search',
    'search_plugins' => array(
      'entity:taxonomy_term' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:node' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'entity:file' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:user' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'entity:node' => array(
      'result_description' => '',
      'bundles' => array(
        'application_checklist' => 'application_checklist',
        'article' => 'article',
        'page' => 'page',
        'paragraphs_page' => 'paragraphs_page',
        'deadlines' => 0,
        'event' => 0,
        'faqs' => 0,
        'group_visit_dates' => 0,
        'high_school_counselor_feed' => 0,
        'instagram_feed' => 0,
        'instagram_media_item' => 0,
        'instagram_url' => 0,
        'notification_block' => 0,
        'organizational_unit' => 0,
        'parallax_scroll_section' => 0,
        'personnel' => 0,
        'places_to_eat' => 0,
        'accommodations' => 0,
        'program' => 0,
        'quick_facts' => 0,
        'slideshow' => 0,
        'spanish_tour_dates' => 0,
        'stars' => 0,
        'student' => 0,
        'tour_dates' => 0,
        'tuition_line_item' => 0,
        'tumblr_blog' => 0,
        'webform' => 0,
      ),
      'group_by_bundle' => 0,
      'include_unpublished' => 1,
    ),
    'entity:file' => array(
      'result_description' => '',
      'bundles' => array(
        'video' => 0,
        'image' => 0,
        'audio' => 0,
        'document' => 0,
      ),
      'group_by_bundle' => 0,
      'show_scheme' => 0,
      'group_by_scheme' => 0,
      'url_type' => 'entity',
      'image_extra_info' => array(
        'thumbnail' => 'thumbnail',
        'dimensions' => 'dimensions',
      ),
    ),
    'entity:taxonomy_term' => array(
      'result_description' => '',
      'bundles' => array(
        'personnel' => 0,
        'event' => 0,
        'statistics' => 0,
        'accommodations' => 0,
        'faqs' => 0,
        'student_type' => 0,
        'organizational_unit' => 0,
        'tour_type' => 0,
        'places_to_eat' => 0,
        'media_folders' => 0,
        'instagram_hashtags' => 0,
        'instagram_users' => 0,
        'territories' => 0,
        'parallax_page' => 0,
      ),
      'group_by_bundle' => 0,
    ),
    'entity:user' => array(
      'result_description' => '',
    ),
    'insert_plugin' => array(
      'plugin' => 'raw_url',
      'url_method' => '1',
    ),
    'attribute_plugins' => array(
      'accesskey' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'target' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'class' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'rel' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'id' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'title' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'autocomplete' => array(
      'charLimit' => '3',
      'wait' => '350',
      'remoteTimeout' => '10000',
    ),
  );
  $linkit_profile->weight = 0;
  $export['wwu_admissions_link_field'] = $linkit_profile;

  return $export;
}
