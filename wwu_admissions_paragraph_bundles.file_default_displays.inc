<?php
/**
 * @file
 * wwu_admissions_paragraph_bundles.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function wwu_admissions_paragraph_bundles_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__aside__file_field_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'aside',
    'image_link' => '',
  );
  $export['image__aside__file_field_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__call_to_action_image_button__file_field_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'call-to-action_image_button',
    'image_link' => '',
  );
  $export['image__call_to_action_image_button__file_field_image'] = $file_display;

  return $export;
}
