<?php
/**
 * @file
 * wwu_admissions_paragraph_bundles.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wwu_admissions_paragraph_bundles_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_1|paragraphs_item|image_grid|default';
  $field_group->group_name = 'group_1';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_grid';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Image Group 1',
    'weight' => '1',
    'children' => array(
      0 => 'field_image_1',
      1 => 'field_image_2',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Image Group 1',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'image-group-1 field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_1|paragraphs_item|image_grid|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_2|paragraphs_item|image_grid|default';
  $field_group->group_name = 'group_2';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_grid';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Image Group 2',
    'weight' => '2',
    'children' => array(
      0 => 'field_image_3',
      1 => 'field_image_4',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Image Group 2',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'image-group-2 field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_2|paragraphs_item|image_grid|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_3|paragraphs_item|image_grid|default';
  $field_group->group_name = 'group_3';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'image_grid';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Large Image',
    'weight' => '0',
    'children' => array(
      0 => 'field_big_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Large Image',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'big-image field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_3|paragraphs_item|image_grid|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cta_block_link_info|paragraphs_item|cta_block|default';
  $field_group->group_name = 'group_cta_block_link_info';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'cta_block';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Link',
    'weight' => '1',
    'children' => array(
      0 => 'field_cta_link',
      1 => 'field_long_text',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Link',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-cta-block-link-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_cta_block_link_info|paragraphs_item|cta_block|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eb_parallax|field_collection_item|eb_background|form';
  $field_group->group_name = 'group_eb_parallax';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'eb_background';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Parallax',
    'weight' => 0,
    'children' => array(
      0 => 'field_eb_parallax_image',
      1 => 'field_eb_parallax_speed',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'instance_settings' => array(
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'classes' => '',
        'attributes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_eb_parallax|field_collection_item|eb_background|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_quote|paragraphs_item|quote_with_image|default';
  $field_group->group_name = 'group_quote';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'quote_with_image';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Quote',
    'weight' => '1',
    'children' => array(
      0 => 'field_attribution',
      1 => 'field_long_text',
      2 => 'field_paragraphs_name',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Quote',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-quote field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_quote|paragraphs_item|quote_with_image|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Image Group 1');
  t('Image Group 2');
  t('Large Image');
  t('Link');
  t('Parallax');
  t('Quote');

  return $field_groups;
}
