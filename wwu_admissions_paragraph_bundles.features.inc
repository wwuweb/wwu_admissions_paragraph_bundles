<?php
/**
 * @file
 * wwu_admissions_paragraph_bundles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wwu_admissions_paragraph_bundles_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function wwu_admissions_paragraph_bundles_image_default_styles() {
  $styles = array();

  // Exported image style: aside.
  $styles['aside'] = array(
    'label' => 'Aside',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 384,
          'height' => 384,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: call-to-action_image_button.
  $styles['call-to-action_image_button'] = array(
    'label' => 'Call-To-Action Image Button',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 256,
          'height' => 256,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_grid_large.
  $styles['image_grid_large'] = array(
    'label' => 'Image Grid Large',
    'effects' => array(
      11 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 1000,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_grid_small.
  $styles['image_grid_small'] = array(
    'label' => 'Image Grid Small',
    'effects' => array(
      12 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 1000,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function wwu_admissions_paragraph_bundles_node_info() {
  $items = array(
    'paragraphs_page' => array(
      'name' => t('Paragraphs Page'),
      'base' => 'node_content',
      'description' => t('A page built from Paragraphs bundles.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function wwu_admissions_paragraph_bundles_paragraphs_info() {
  $items = array(
    'accolade_with_image' => array(
      'name' => 'Accolade with Image',
      'bundle' => 'accolade_with_image',
      'locked' => '1',
    ),
    'accordion' => array(
      'name' => 'Accordion',
      'bundle' => 'accordion',
      'locked' => '1',
    ),
    'cta_block' => array(
      'name' => 'Call-To-Action Block',
      'bundle' => 'cta_block',
      'locked' => '1',
    ),
    'cta_button' => array(
      'name' => 'Call-To-Action Button',
      'bundle' => 'cta_button',
      'locked' => '1',
    ),
    'cta_buttons' => array(
      'name' => 'Call-To-Action Buttons',
      'bundle' => 'cta_buttons',
      'locked' => '1',
    ),
    'cta_image_button' => array(
      'name' => 'Call-To-Action Image Button',
      'bundle' => 'cta_image_button',
      'locked' => '1',
    ),
    'cta_image_buttons' => array(
      'name' => 'Call-To-Action Image Buttons',
      'bundle' => 'cta_image_buttons',
      'locked' => '1',
    ),
    'heading' => array(
      'name' => 'Heading',
      'bundle' => 'heading',
      'locked' => '1',
    ),
    'icon_link' => array(
      'name' => 'Icon Link',
      'bundle' => 'icon_link',
      'locked' => '1',
    ),
    'image_grid' => array(
      'name' => 'Image Grid',
      'bundle' => 'image_grid',
      'locked' => '1',
    ),
    'instagram_block' => array(
      'name' => 'Instagram Block',
      'bundle' => 'instagram_block',
      'locked' => '1',
    ),
    'long_text' => array(
      'name' => 'Long Text',
      'bundle' => 'long_text',
      'locked' => '1',
    ),
    'parallax' => array(
      'name' => 'Parallax',
      'bundle' => 'parallax',
      'locked' => '1',
    ),
    'quote_with_image' => array(
      'name' => 'Quote with Image',
      'bundle' => 'quote_with_image',
      'locked' => '1',
    ),
    'stat' => array(
      'name' => 'Stat',
      'bundle' => 'stat',
      'locked' => '1',
    ),
  );
  return $items;
}
