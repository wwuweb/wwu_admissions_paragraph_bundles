<?php
/**
 * @file
 * wwu_admissions_paragraph_bundles.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wwu_admissions_paragraph_bundles_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view paragraph content accolade_with_image'.
  $permissions['view paragraph content accolade_with_image'] = array(
    'name' => 'view paragraph content accolade_with_image',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content accordion'.
  $permissions['view paragraph content accordion'] = array(
    'name' => 'view paragraph content accordion',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content cta_block'.
  $permissions['view paragraph content cta_block'] = array(
    'name' => 'view paragraph content cta_block',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content cta_button'.
  $permissions['view paragraph content cta_button'] = array(
    'name' => 'view paragraph content cta_button',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content cta_buttons'.
  $permissions['view paragraph content cta_buttons'] = array(
    'name' => 'view paragraph content cta_buttons',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content cta_image_button'.
  $permissions['view paragraph content cta_image_button'] = array(
    'name' => 'view paragraph content cta_image_button',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content cta_image_buttons'.
  $permissions['view paragraph content cta_image_buttons'] = array(
    'name' => 'view paragraph content cta_image_buttons',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content heading'.
  $permissions['view paragraph content heading'] = array(
    'name' => 'view paragraph content heading',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content icon_link'.
  $permissions['view paragraph content icon_link'] = array(
    'name' => 'view paragraph content icon_link',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content image_grid'.
  $permissions['view paragraph content image_grid'] = array(
    'name' => 'view paragraph content image_grid',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content instagram_block'.
  $permissions['view paragraph content instagram_block'] = array(
    'name' => 'view paragraph content instagram_block',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content long_text'.
  $permissions['view paragraph content long_text'] = array(
    'name' => 'view paragraph content long_text',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content parallax'.
  $permissions['view paragraph content parallax'] = array(
    'name' => 'view paragraph content parallax',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content quote_with_image'.
  $permissions['view paragraph content quote_with_image'] = array(
    'name' => 'view paragraph content quote_with_image',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content stat'.
  $permissions['view paragraph content stat'] = array(
    'name' => 'view paragraph content stat',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  return $permissions;
}
